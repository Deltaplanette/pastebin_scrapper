import requests
import time
from bs4 import BeautifulSoup
import re
import os
import json

class Pastebin:

    def get_session(self):
        session = requests.session()
        # Tor uses the 9050 port as the default socks port
        session.proxies = {'http': 'socks5://127.0.0.1:9050',
                           'https': 'socks5://127.0.0.1:9050'}
        return session

    def check_if_page_up(self, url):
        session = self.get_session()
        page = session.get(url).text
        return page

    def get_scrapping_url_archive(self, page):
        tableau = []
        # We exclude this links for the parsing
        exception = ["/pro", "/api", "/tools", "/faq", "https://deals.pastebin.com", "/login", "/signup", "/archive",
                     "/archive/lua", "/archive/javascript", "/archive/csharp", "/archive/javascript",
                     "/archive/html4strict", "/archive/python", "/archive/bash", "/archive/json", "/archive/csharp",
                     "/archive/cpp", "/doc_cookies_policy", "/tools#chrome", "/tools#firefox", "/tools#iphone",
                     "/tools#windows", "/tools#android", "/tools#macos", "/tools#opera", "/tools#pastebincl",
                     "/", "/languages", "/night_mode", "/doc_scraping_api", "/doc_privacy_statement",
                     "/doc_terms_of_service", "/doc_security_disclosure", "/dmca", "/contact", "/archive/java",
                     "http://creativecommons.org/licenses/by-sa/3.0/", "https://favpng.com",
                     "http://steadfast.net/services/dedicated-servers.php?utm_source=pastebin.com&utm_medium=referral"
                     "&utm_content=footer_link_dedicated_server_hosting_by&utm_campaign"
                     "=referral_20140118_x_x_pastebin_partner&source=referral_20140118_x_x_pastebin_partner",
                     "http://steadfast.net/?utm_source=pastebin.com&utm_medium=referral&utm_content"
                     "=footer_link_steadfast&utm_campaign=referral_20140118_x_x_pastebin_partner&source"
                     "=referral_20140118_x_x_pastebin_partner",
                     "https://facebook.com/pastebin",
                     "https://twitter.com/pastebin",
                     "/archive/xml",
                     "/archive/c",
                     "/archive/apache",
                     "/archive/php",
                     "/archive/powershell",
                     "/archive/css",
                     "#0",
                     ]
        # We get all the links in the page then exclude the useless links
        try:
            soup = BeautifulSoup(page, features="html.parser")
            for a in soup.find_all('a', href=True):
                if a['href'] not in exception:
                    tableau.append(a['href'])
            return tableau
        except TypeError:
            print("Error")
            pass

    def create_link(self, results):
        tableau = []
        pastebin = "https://pastebin.com"
        for i in results:
            url = ''.join([pastebin, i])
            tableau.append(url)
        return tableau

    def parse_page(self, urls):
        tableau = []
        domaines = []
        dict = {}
        tab = ["orange", "livebox", "stephane richard", "business services", "OBS", "OCD", "AOB"]
        for i in urls:
            time.sleep(1)
            soup = BeautifulSoup(p.check_if_page_up(i), features="html.parser")
            print(i)
            try:
                for j in tab:
                    if j in str(soup).lower():
                        for textarea in soup.find('textarea'):
                            print("manureva")
                            dict["timestamp"] = time.time()
                            dict["ipConcerned"] = re.findall(
                                "(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})", textarea)
                            dict["mailAddress"] = re.findall("[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,3}", textarea)
                            domains = re.findall("([a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])\.)*orange\.com$",
                                                 textarea)
                            for k in domains:
                                hostname = k[0]
                                response = os.system("ping -n 1 " + str(hostname))
                                if response == 0:
                                    domaines.append(k[0])
                                else:
                                    pass
                            dict["domainName"] = domaines
                            dict["cuid"] = re.findall("[A-Z]{4}[0-9]{4}", textarea)
                            dict["url"] = i
                            tableau.append(dict)
            except TypeError:
                pass
        return tableau


if __name__ == '__main__':
    proxy = False
    p = Pastebin()
    while True:
        page = p.check_if_page_up('https://pastebin.com/archive')
        results = p.get_scrapping_url_archive(page)
        urls = p.create_link(results)
        for i in p.parse_page(urls):
            print(json.dumps(i))

