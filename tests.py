"""import re

strings = "orange le100@orange.com fic.orange.com Orange 10.201.100.201"
print(re.findall("(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})",strings))
print(re.findall("[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,3}", strings))
print(re.findall("(([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+[a-z]{2,3})", strings))
print(re.findall("[A-Z]{4}[0-9]{4}", strings))

stringou = "La est belle stephane richard"


tab = ["orange", "livebox", "stephane richard", "business services", "OBS", "OCD", "AOB"]
for i in tab:
    if i in stringou.lower():
        print(1)
    else:
        print(0)"""

import requests

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session
session = get_tor_session()
while True:
    print(session.get("https://pastebin.com/archives").text)
    # Above should print an IP different than your public IP

    # Following prints your normal public IP
    print(requests.get("http://httpbin.org/ip").text)